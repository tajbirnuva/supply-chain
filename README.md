**Supply Chain is a management type application aimed at fulfilling the following requirements.**

The supply chain management system we are going to develop will only contain the flow of distributing from company’s inventory to dealers. The dealer will be registered by filling up the dealer registration form, but will be approved by a dealer approver inside the company's side. For registration, the dealer will have to provide the dealer's NID scanned copy, TIN certificate scanned copy as the mandatory documents including his other information. The dealer approver will cross match his NID and TIN Certificates scanned copies in the database. After the approver approves, the dealer will be able to create a requisition to the company’s inventory manager. While creating the requisition, he will provide the transaction ID, payment method name, account number and initially paid amount as mandatory field. If he has due, he will have to pay the due before the next requisition. The inventory manager will then forward the transaction ID and initially paid amount to the Accounts manager. Once the accounts manager confirms the amount received, the inventory manager will be able to send the product to that dealer. To provide the due, the dealer will have to fill up the due payment form with the transaction ID, payment method name, account number and payment amount, which will be directly submitted to the accounts manager.

The delivery process will be taken part by initiating a load requisition submitted by delivery in charge. He will submit the item type(cartoon size * cartons per lot). After submitting this requisition, when the inventory manager approves the lot, the delivery in charge will be good to go. When the Delivery in charge delivers the lot to the dealer, he will request for the consent through the application. Once he approves, the delivery process is finalized.

---------------------------------------------------------------------------------------------------------------------------------------------------
**Setup Process:**
1. The application is a Maven project. Therefore, Maven must have to present in the dev environment.
2. The application is developed with Intellij Idea. Therefore, it is preferred to use intellij for importing easily.
3. The development environment JDK version is JDK 11 (Amazon Corretto)
4. Database is MySQL. DB username is `root` and password is `1234`. If the connection is properly configured, database will be created and initialized with seed data automatically.

---------------------------------------------------------------------------------------------------------------------------------------------------
**Users & Their Role:**
| Username | Password | Role |
| ------ | ------ | ------ |
| dealer@gmail.com | 12345 | Dealer |
| admin@gmail.com | 12345 | Admin |
| inmanager@gmail.com | 12345 | Inventory Manager |
| accmanager@gmail.com | 12345 | Accounts Manager |
| delivery@gmail.com | 12345 | Delivery Incharge |

---------------------------------------------------------------------------------------------------------------------------------------------------

