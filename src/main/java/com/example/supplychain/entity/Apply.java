package com.example.supplychain.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "apply")
@Getter
@Setter
public class Apply implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long applyId;
    @Column
    private String username;
    @Column
    private String password;
    @Column
    private String nidImage;
    @Column
    private String tinImage;
    @Column
    private String name;
    @Column
    private String shop;
    @Column
    private String address;
    @Column
    private String mobile;

    @Column
    private String status = "pending";
}
