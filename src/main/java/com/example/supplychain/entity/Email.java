package com.example.supplychain.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class Email implements Serializable {
    private long requiredId;
    private String emailSubject;
    private String emailBody;
}
