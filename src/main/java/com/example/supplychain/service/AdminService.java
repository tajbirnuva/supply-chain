package com.example.supplychain.service;

import com.example.supplychain.entity.Apply;
import com.example.supplychain.entity.Email;
import com.example.supplychain.entity.Role;
import com.example.supplychain.entity.User;
import com.example.supplychain.repository.ApplyRepo;
import com.example.supplychain.repository.RoleRepo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AdminService {
    private final ApplyRepo applyRepo;
    private final EmailService emailService;
    private final UserService userService;
    private final RoleRepo roleRepo;

    public AdminService(ApplyRepo applyRepo, EmailService emailService,
                        UserService userService, RoleRepo roleRepo) {
        this.applyRepo = applyRepo;
        this.emailService = emailService;
        this.userService = userService;
        this.roleRepo = roleRepo;
    }


    public List<Apply> getAllApplyList() {
        return applyRepo.findAllByStatus("pending");
    }

    public Apply getApplyDetails(long applyId) {
        return applyRepo.getOne(applyId);
    }

    public void dealerApprovalReject(Email email) {

        /*----------Email's requiredId = applyId---------*/

        Apply apply = applyRepo.getOne(email.getRequiredId());
        apply.setStatus("Reject");
        applyRepo.save(apply);
        this.dealerShipApprovalMail(email.getEmailSubject(), email.getEmailBody());
    }

    public void dealerApprovalConfirm(long applyId) {
        Apply apply = applyRepo.getOne(applyId);
        apply.setStatus("Confirm");
        applyRepo.save(apply);
        this.createDealerAccount(apply);

        this.dealerShipApprovalMail("Dealership Confirmation",
                this.confirmationMailMessage(apply));
    }

    private void createDealerAccount(Apply apply) {
        Role role = roleRepo.getOne(1L);
        List<Role> roleList = new ArrayList<>();
        roleList.add(role);
        User user = new User();
        user.setUsername(apply.getUsername());
        user.setPassword(apply.getPassword());
        user.setRoleList(roleList);
        userService.save(user);
    }


    /*--------------------HELPER METHOD--------------------------*/
    private void dealerShipApprovalMail(String subject, String body) {
        emailService.sendEmail("tajbirnuva@gmail.com", subject, body);
    }

    private String confirmationMailMessage(Apply apply) {
        return "Dealership Registration Successfully Confirm\n" +
                "--------------------------------------\n" + "Given Information :\n" +
                "Owner  : " + apply.getName() + "\n" +
                "Mobile : " + apply.getMobile() + "\n" +
                "Shop : " + apply.getShop() + "\n" +
                "Address : " + apply.getAddress() + "\n" +
                "--------------------------------------\n" +
                "Mr." + apply.getName() + " Now You Can Login Our System by this Email -" + apply.getUsername() + " Address";
    }
}
