package com.example.supplychain.service;

import com.example.supplychain.entity.Category;
import com.example.supplychain.entity.Item;
import com.example.supplychain.entity.Requisition;
import com.example.supplychain.repository.CategoryRepo;
import com.example.supplychain.repository.ItemRepo;
import com.example.supplychain.repository.RequisitionRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InventoryService {
    private final CategoryRepo categoryRepo;
    private final ItemRepo itemRepo;
    private final RequisitionRepo requisitionRepo;

    public InventoryService(CategoryRepo categoryRepo, ItemRepo itemRepo, RequisitionRepo requisitionRepo) {
        this.categoryRepo = categoryRepo;
        this.itemRepo = itemRepo;
        this.requisitionRepo = requisitionRepo;
    }


    public void saveCategory(Category category) {
        categoryRepo.save(category);
    }

    public List<Category> getAllCategory() {
        return categoryRepo.findAll();
    }

    public String saveItem(Item item) {
        if (itemRepo.findByItemName(item.getItemName()) != null) {
            return "Product Already Exist";
        } else {
            itemRepo.save(item);
            return String.valueOf(itemRepo.findByItemName(item.getItemName()).getItemId());
        }
    }

    public Category getCategory(long categoryId) {
        return categoryRepo.getOne(categoryId);
    }

    public Item getItemDetails(long itemId) {
        return itemRepo.getOne(itemId);
    }

    public int updateItemStockQuantity(Item item) {
        Item item1 = itemRepo.getOne(item.getItemId());
        item1.setItemQuantity(item1.getItemQuantity() + item.getItemQuantity());
        itemRepo.save(item1);
        return item1.getItemQuantity();
    }

    public Requisition getRequisitionDetails(long requisitionId) {
        return requisitionRepo.getOne(requisitionId);
    }

    public void sendRequisitionToAccountManager(long requisitionId) {
        Requisition requisition = this.getRequisitionDetails(requisitionId);
        requisition.setStatus("Send To Account Manager");
        requisitionRepo.save(requisition);
    }

    public void sendRequisitionToDeliveryIncharge(long requisitionId) {
        Requisition requisition = this.getRequisitionDetails(requisitionId);
        requisition.setStatus("Send To Delivery Incharge");
        requisitionRepo.save(requisition);
    }
}
