package com.example.supplychain.service;

import com.example.supplychain.entity.Requisition;
import com.example.supplychain.repository.RequisitionRepo;
import org.springframework.stereotype.Service;

@Service
public class AccountManagerService {
    private final RequisitionRepo requisitionRepo;

    public AccountManagerService(RequisitionRepo requisitionRepo) {
        this.requisitionRepo = requisitionRepo;
    }


    public void sendRequisitionToInventoryManager(long requisitionId) {
        Requisition requisition = this.getRequisitionDetails(requisitionId);
        requisition.setStatus("Ready To Delivery");
        requisitionRepo.save(requisition);
    }

    public Requisition getRequisitionDetails(long requisitionId) {
        return requisitionRepo.getOne(requisitionId);
    }
}
