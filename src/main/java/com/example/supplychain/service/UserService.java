package com.example.supplychain.service;

import com.example.supplychain.entity.User;
import com.example.supplychain.repository.UserRepo;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements UserDetailsService {
    private final UserRepo userRepo;

    public UserService(UserRepo userRepo) {
        this.userRepo = userRepo;
    }


    public void save(User user) {
        user.setPassword(passCustomEncoder().encode(user.getPassword()));
        userRepo.save(user);
    }

    public void saveAll(List<User> userList) {
        userList.forEach(user -> {
            user.setPassword(passCustomEncoder().encode(user.getPassword()));
        });
        userRepo.saveAll(userList);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User Not Found");
        }
        return user;
    }


    /*---------------------HELPER METHOD----------------------*/

    public PasswordEncoder passCustomEncoder() {
        return new BCryptPasswordEncoder();
    }
}
