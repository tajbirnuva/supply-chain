package com.example.supplychain.repository;

import com.example.supplychain.entity.Wishlist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WishlistRepo extends JpaRepository<Wishlist, Long> {
    @Query
    List<Wishlist> findAllByEnableTrueAndUser_UserId(long userId);
}
