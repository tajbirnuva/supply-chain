package com.example.supplychain.dto;

import com.example.supplychain.entity.Wishlist;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class RequisitionDto implements Serializable {
    private long requisitionId;
    private String requisitionNo;
    private List<Wishlist> productList;

    private double payable;
    private double paid;
    private String paymentStatus;
    private String status;
    private String dealer;


}
