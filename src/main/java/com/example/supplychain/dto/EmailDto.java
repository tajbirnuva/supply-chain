package com.example.supplychain.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class EmailDto implements Serializable {
    private long requiredId;
    private String emailSubject;
    private String emailBody;

}
