package com.example.supplychain.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ApplyDto implements Serializable {
    private long applyId;
    private String username;
    private String password;
    private String name;
    private String mobile;
    private String shop;
    private String address;

    private String nidImagePath;
    private String tinImagePath;
}
