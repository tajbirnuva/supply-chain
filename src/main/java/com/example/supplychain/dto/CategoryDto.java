package com.example.supplychain.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class CategoryDto implements Serializable {
    private long categoryId;
    private String categoryName;

    List<ItemDto> itemDtoList;
}
