package com.example.supplychain.dto;

import com.example.supplychain.entity.Item;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class WishlistDto implements Serializable {

    private long wishlistId;
    private Item item;
    private int orderQuantity;
    private double orderQuantityPrice;

    private String itemName;
}
