package com.example.supplychain.controller;

import com.example.supplychain.dto.ApplyDto;
import com.example.supplychain.dto.EmailDto;
import com.example.supplychain.entity.Apply;
import com.example.supplychain.entity.Email;
import com.example.supplychain.service.AdminService;
import com.example.supplychain.service.EmailService;
import org.springframework.beans.BeanUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private final AdminService adminService;
    private final EmailService emailService;

    public AdminController(AdminService adminService, EmailService emailService) {
        this.adminService = adminService;
        this.emailService = emailService;
    }


    @GetMapping("/approval")
    public String getDealerApprovalPage(Model model) {
        model.addAttribute("title", "Dealer Approval");
        model.addAttribute("emailDto", new EmailDto());
        model.addAttribute("applyDtoList", this.getAllApplyList());
        return "admin/approval";
    }


    @GetMapping(value = "/get-apply-details/{applyId}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ApplyDto getApplyDetails(@PathVariable("applyId") long applyId) {
        Apply apply = adminService.getApplyDetails(applyId);
        ApplyDto applyDto = new ApplyDto();

        applyDto.setNidImagePath("/Apply Photo/" + apply.getApplyId() + "/" + apply.getNidImage());
        applyDto.setTinImagePath("/Apply Photo/" + apply.getApplyId() + "/" + apply.getTinImage());
        return applyDto;
    }


    @GetMapping("/confirm/{applyId}")
    public String dealerApprovalConfirm(@PathVariable("applyId") long applyId, RedirectAttributes redirectAttributes) {
        adminService.dealerApprovalConfirm(applyId);
        redirectAttributes.addFlashAttribute("message", "Send Confirmation Email To Dealer");
        return "redirect:/admin/approval";
    }


    @PostMapping("/reject")
    public String dealerApprovalReject(@ModelAttribute EmailDto emailDto, RedirectAttributes redirectAttributes) {
        Email email=new Email();
        BeanUtils.copyProperties(emailDto,email);
        adminService.dealerApprovalReject(email);

        redirectAttributes.addFlashAttribute("message", "Send Rejection Email To Dealer");
        return "redirect:/admin/approval";
    }

    /*--------------------------HELPER METHOD------------------------------*/

    private List<ApplyDto> getAllApplyList() {
        List<Apply> applyList = adminService.getAllApplyList();
        List<ApplyDto> applyDtoList = new ArrayList<>();
        for (Apply apply : applyList) {
            ApplyDto applyDto = new ApplyDto();
            BeanUtils.copyProperties(apply, applyDto);
            applyDtoList.add(applyDto);
        }
        return applyDtoList;
    }
}
