package com.example.supplychain.controller;

import com.example.supplychain.dto.RequisitionDto;
import com.example.supplychain.dto.TransactionDto;
import com.example.supplychain.entity.Requisition;
import com.example.supplychain.entity.Transaction;
import com.example.supplychain.service.AccountManagerService;
import com.example.supplychain.service.DealerService;
import com.example.supplychain.service.EmailService;
import com.example.supplychain.service.InventoryService;
import org.springframework.beans.BeanUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/ac-manager")
public class AccountManagerController {

    private final InventoryService inventoryService;
    private final DealerService dealerService;
    private final AccountManagerService acManagerService;
    private final EmailService emailService;

    public AccountManagerController(InventoryService inventoryService, DealerService dealerService,
                                    AccountManagerService acManagerService, EmailService emailService) {
        this.inventoryService = inventoryService;
        this.dealerService = dealerService;
        this.acManagerService = acManagerService;
        this.emailService = emailService;
    }


    @GetMapping("/requisition")
    public String getRequisitionPage(Model model) {
        model.addAttribute("title", "Requisition Transaction Check");
        model.addAttribute("requisitionDtoList", this.getRequisitionDtoList("Non Confirm"));
        return "account-manager/requisition-list";
    }

    @GetMapping("/due-payment-requisition")
    public String getDuePaymentRequisitions(Model model) {
        model.addAttribute("title", "Due Payment Requisitions");
        model.addAttribute("requisitionDtoList", this.getRequisitionDtoList("Partial Paid Confirmed"));
        return "account-manager/due-payment-requisitions";
    }

    @GetMapping("/confirmed-requisition")
    public String getConfirmedRequisitions(Model model) {
        model.addAttribute("title", "Confirmed Requisitions");
        model.addAttribute("requisitionDtoList", this.getRequisitionDtoList("Full Paid Confirmed"));
        return "account-manager/confirmed-requisitions";
    }

    @GetMapping(value = "/payment-details/{requisitionId}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<TransactionDto> getRequisitionPaymentDetails(@PathVariable("requisitionId") long requisitionId) {
        Requisition requisition = inventoryService.getRequisitionDetails(requisitionId);
        List<TransactionDto> transactionDtoList = new ArrayList<>();
        for (Transaction transaction : requisition.getTransactionList()) {
            TransactionDto transactionDto = new TransactionDto();
            BeanUtils.copyProperties(transaction, transactionDto);
            transactionDto.setRequisitionNo(transaction.getRequisition().getRequisitionNo());
            transactionDtoList.add(transactionDto);
        }
        return transactionDtoList;
    }


    @GetMapping("/payment-clearance/{requisitionId}")
    public String sendRequisitionToInventoryManager(@PathVariable(name = "requisitionId") long requisitionId,
                                                    RedirectAttributes redirectAttributes) {
        acManagerService.sendRequisitionToInventoryManager(requisitionId);
        redirectAttributes.addFlashAttribute("message", "Requisition Payment Clearance Send To Inventory Manager");
        return "redirect:/ac-manager/requisition";
    }


    @GetMapping("/email/{requisitionNo}")
    public String sendEmailToDealer(@PathVariable(name = "requisitionNo") String requisitionNo,
                                    RedirectAttributes redirectAttributes) {
        emailService.sendEmail("tajbirnuva@gmail.com",
                "Due Payment",
                "Please Pay Your Due Payment for " + requisitionNo);
        redirectAttributes.addFlashAttribute("message", "Email Send");
        return "redirect:/ac-manager/requisition";
    }

    /*-------------------------HELPER METHOD-----------------------------*/
    private List<RequisitionDto> getRequisitionDtoList(String status) {
        List<Requisition> requisitionList = dealerService.getDealerRequisitionList(status);
        List<RequisitionDto> requisitionDtoList = new ArrayList<>();
        for (Requisition requisition : requisitionList) {
            RequisitionDto requisitionDto = new RequisitionDto();
            BeanUtils.copyProperties(requisition, requisitionDto);
            requisitionDto.setDealer(requisition.getUser().getUsername());
            requisitionDtoList.add(requisitionDto);
        }
        return requisitionDtoList;
    }
}
