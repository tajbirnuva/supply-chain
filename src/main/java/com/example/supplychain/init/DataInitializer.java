package com.example.supplychain.init;


import com.example.supplychain.entity.Role;
import com.example.supplychain.entity.User;
import com.example.supplychain.repository.RoleRepo;
import com.example.supplychain.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class DataInitializer implements CommandLineRunner {

    @Autowired
    RoleRepo roleRepo;
    @Autowired
    UserService userService;

    @Override
    public void run(String... args) throws Exception {
        List<Role> roleList = Arrays.asList(
                new Role("Dealer"),
                new Role("Admin"),
                new Role("Inventory Manager"),
                new Role("Accounts Manager"),
                new Role("Delivery Incharge")
        );

        /*roleRepo.saveAll(roleList);*/

        /*------------------------NEW USERS WITH THEIR ROLE------------------------*/
        List<Role> roleList1 = Arrays.asList(roleRepo.getOne(1L));
        List<Role> roleList2 = Arrays.asList(roleRepo.getOne(2L));
        List<Role> roleList3 = Arrays.asList(roleRepo.getOne(3L));
        List<Role> roleList4 = Arrays.asList(roleRepo.getOne(4L));
        List<Role> roleList5 = Arrays.asList(roleRepo.getOne(5L));

        List<User> userList = Arrays.asList(
                new User("dealer2@gmail.com", "12345", roleList1),
                new User("admin@gmail.com", "12345", roleList2),
                new User("inmanager@gmail.com", "12345", roleList3),
                new User("accmanager@gmail.com", "12345", roleList4),
                new User("delivery@gmail.com", "12345", roleList5)
        );

        /*userService.saveAll(userList);*/

    }
}
